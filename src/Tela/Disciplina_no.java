
package Tela;

public class Disciplina_no {
       
    private Disciplina_no proximo;
    private Disciplinas disciplina;

    public Disciplina_no(Disciplinas disciplina) {
        this.proximo = null;
        this.disciplina = disciplina;
    }
    
    
    
    
    public Disciplina_no getProximo() {
        return proximo;
    }

    public void setProximo(Disciplina_no proximo) {
        this.proximo = proximo;
    }

    public Disciplinas getDisciplinas() {
        return disciplina;
    }

    public void setDisciplinas(Disciplinas disciplina) {
        this.disciplina = disciplina;
    }
    
    
    
}
