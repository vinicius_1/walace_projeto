
package Tela;

public class Lista_disciplina {

    private Disciplina_no primeiro;
    private Disciplina_no ultimo;
    private int tamanho;

    public Lista_disciplina() {
        this.primeiro = null;
        this.ultimo = null;
        this.tamanho = 0;
    }

    
    public Disciplina_no getPrimeiro() {
        return primeiro;
    }

    public void setPrimeiro(Disciplina_no primeiro) {
        this.primeiro = primeiro;
    }

    public Disciplina_no getUltimo() {
        return ultimo;
    }

    public void setUltimo(Disciplina_no ultimo) {
        this.ultimo = ultimo;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }
    
    
    public boolean estaVazia(){
        
        return (this.primeiro == null);
    
        }
    
    
    public boolean adicionar(Disciplinas disciplina){
        
        Disciplina_no novo = new Disciplina_no(disciplina);
        
        if(estaVazia()){
            this.primeiro = novo;        
        }
        
        
        else{
            this.ultimo.setProximo(novo);     
        }
        this.ultimo = novo;
        this.tamanho++;
        return true;
   
    }
    
    public String mostrar(){
          
        String msg = "";
         if(estaVazia()){
           msg = "Nenhuma disciplina cadastrada até o momento";
            return msg;
        }
         
         else{
         Disciplina_no atual = this.primeiro;
         
        for(int i = 0; i < this.tamanho; i++){  
            
            msg += (i + 1) + "º" +atual.getDisciplinas().getDisciplina() + " ->";
                                  
            atual = atual.getProximo();        
        }
        
         }
         return msg + "\n";
    }

    
}
