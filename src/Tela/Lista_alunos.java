
package Tela;

import static java.lang.Integer.parseInt;
import java.util.Scanner;
import static jdk.nashorn.internal.objects.NativeString.toLowerCase;

public class Lista_alunos {
    
    private Alunos alunos[] = new Alunos[60];
    private int tamanho;

    public Lista_alunos() {
        this.tamanho = 0;
    }  
    
    public Alunos[] getAluno() {
        return alunos;
    }

    public void setAluno(Alunos[] aluno) {
        this.alunos = aluno;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }
    
    
    
    public boolean isEmpity(){
    
        return (this.tamanho == 0);
    }
    
    
    public boolean isFull(){
    
        return (this.tamanho == this.alunos.length);
    }
    
    
    public boolean comparar(Alunos aluno1, Alunos aluno2){
        return (aluno1.getRgm() == aluno2.getRgm());
    }
          
    
    public int buscaBinaria(int rgm){
        int fim = this.tamanho - 1;
        int inicio = 0;
        int meio;
        
        if(isEmpity())
            return -1;
			

         while(inicio <= fim){
             
            meio = (int) (inicio + fim) / 2;
            
            if(this.alunos[meio].getRgm() > rgm){
                fim =  meio - 1;
            }
            
            else if (this.alunos[meio].getRgm() < rgm){
               inicio = meio + 1;
            }
            
            else
                return meio;
         }
        
            return -1;
    }
    
    public void deslocarParaEsquerda(int pos) {
		
		for(int i = pos; i < this.tamanho; i++) {			
			this.alunos[i] = this.alunos[i + 1];                                
		}
	}
    
    
    public void deslocarParaDireita(int pos) {
		
		for(int i = tamanho; i > pos; i--) {			
			alunos[i] = alunos[i - 1];                                
		}
		
	}
	
    
     public boolean cadastrar(Alunos aluno){  
           
          
            if(isFull()){
                return false;
            }
            
          
           
            for(int i = 0; i <= this.tamanho; i++){             
                
                
                Scanner confirmar = new Scanner(System.in); 
                String receber;
                
                Scanner disciplina = new Scanner(System.in);
                String nova_dis;
                                                               
                aluno.dis = new Lista_disciplina();//instanciando um objeto da lista disciplina
                
                do{
                     
                    Disciplinas nova_disciplina = new Disciplinas();//Criando um objeto da classe disciplina
                                                                                                                  
                     System.out.println("Mais disciplina?");
                     receber = confirmar.nextLine();
                                                                                        
                     if(!receber.equalsIgnoreCase("nao")){
                        
                          System.out.println("Digite a disciplina: ");
                          nova_dis = disciplina.nextLine();
                            
                          nova_disciplina.setDisciplina(nova_dis);//passando o nome da disciplina escrita pelo user
                          
                          
                          aluno.dis.adicionar(nova_disciplina);//adicionando a disciplina a nossa lista                                                                                             
                                              
                      }
                                            
                 }while(!receber.equalsIgnoreCase("nao"));
                
                
                 if(!isEmpity()){
                 for(int j = 0; j < this.tamanho; j++){
                    if(aluno.getRgm() < this.alunos[j].getRgm()){
                           
                           deslocarParaDireita(j);
                           this.alunos[j] = aluno;
                           this.tamanho++;
                           return true;
                           
                        }
                    }
                 }
                 
                 this.alunos[this.tamanho] = aluno;
                 this.tamanho++;
                 return true;
          }           
                             
                    
            
         return true;           
    }


    
    public boolean mostrar_aluno(int rgm){
        
       if(isEmpity()){
           return false;
       }
          
        int valor = buscaBinaria(rgm);
            
            if(valor == -1){
                System.out.println("RGM não cadastrado");
                return false;
            }
            else{
             System.out.println("--------Mostrando aluno pelo RGM---------");  
             System.out.println("Aluno: " + this.alunos[valor].getNome() + 
                                "\n RGM: " + this.alunos[valor].getRgm()
                                + "\nDisciplinas: " + this.alunos[valor].dis.mostrar());
             return true;
            }               
         
    }
    
    
    public void exibirLista() {
		
		for (int i = 0; i < this.tamanho; i++) {
			
		System.out.println((i + 1) + "º Aluno " + 
                                  "\nNome: " + this.alunos[i].getNome() +
                                  "\nRGM: " + this.alunos[i].getRgm() +
                                  "\nDisciplinas:" + this.alunos[i].dis.mostrar() + "\n");
			
		}

            }
     
    
    public boolean remover (int rgm) { 
       
        if(isEmpity()){
            return false;
        }
        
      int valor = buscaBinaria(rgm);
      
      if(valor == -1){
          System.out.println("RGM não cadastrado");
          return false;
      }
          else{
              deslocarParaEsquerda(valor);
              this.tamanho--;
              return true;             
            }
     }
    
}
